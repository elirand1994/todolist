import {ITodo, ListOption, CommandOption} from '../types/index.js'
import {readFileReturnDataAsArrayObjects, stringifyDataAndWriteToFile} from '../db/DB.js'
import {generateID} from '../utils/IdGenerator.js'
import {printDataFromTodoArray} from '../view/UI.js'


//add
export async function addFunction(todoFromUser: string, currentTodos:ITodo[]) {
      let task: ITodo = {
        id: generateID(),
        todo: todoFromUser,
        active: true,
      };
  
      currentTodos.push(task);
      // await stringifyDataAndWriteToFile(jsonPath, dataFile);
      return currentTodos
  }
  
  // list of todos
  export async function listFunction( flag: string, currentTodos:ITodo[]) {    
    const flagNoDash = flag.slice(1)   
    let filtered : ITodo[] = []
    switch (flagNoDash) {
      case ListOption.a || ListOption.all: {
        filtered = currentTodos
        break;
      }
  
      case ListOption.o || ListOption.open: {
        filtered = currentTodos.filter((t) => t.active === true);
        break;
      }
  
      case ListOption.c || ListOption.completed: {
        filtered = currentTodos.filter((t) => t.active === false);
        break;
      }
    }    
    await printDataFromTodoArray(filtered);
    return currentTodos
  }
  
  // delete todo
  export async function deleteFunction( param: string, currentTodos:ITodo[]) {
    // const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
    console.log("param task id: ", param);
    console.log("task before delete ", currentTodos);

    const ids: string[] = currentTodos.map((x) => x.id);
    let afterDelete: ITodo[] = [];
    if (param === "-completed") {
      afterDelete = currentTodos.filter((t) => t.active === true);
    } else if (ids.includes(param)) {
      console.log("found task id");
      console.log("param task id: ", param);
      
      afterDelete = currentTodos.filter((t) => t.id !== param);
    }
  
    // await stringifyDataAndWriteToFile(jsonPath, afterDelete);
    return afterDelete
  }
  
  //mark task as completed
  export async function doneFunction( todoId: string, currentTodos:ITodo[]) {
    // const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
    const afterChange: ITodo[] = currentTodos.map((t) => {
      if (t.id === todoId) {
        t.active = false;
        return t;
      } else {
        return t;
      }
    });
  
    // await stringifyDataAndWriteToFile(jsonPath, afterChange);
    return afterChange
  }
  
  //mark task as open - not completed
  export async function openFunction( todoId: string, currentTodos:ITodo[]) {
    // const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
    const afterChange: ITodo[] = currentTodos.map((t) => {
      if (t.id === todoId) {
        t.active = true;
        return t;
      } else {
        return t;
      }
    });
  
    // await stringifyDataAndWriteToFile(jsonPath, afterChange);
    return afterChange
  }