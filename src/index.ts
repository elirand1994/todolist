import log from "@ajar/marker";
import fs from "fs/promises";
import { constants } from "fs";
import {ITodo,ListOption, CommandOption} from './types/index.js'

(async () => {
  const args: string[] = process.argv;
  const cwd: string = args[1].slice(0, -13); //slice src/index.js which is 13 chars
  const jsonPath: string = `${cwd}data/todos-data.json`;


  //call args parser from utils

  switch (args[2]) {
    case CommandOption.add: {
      addFunction(jsonPath, args[3]);
      break;
    }

    case CommandOption.ls: {
      listFunction(jsonPath, args[3]);
      break;
    }

    case CommandOption.del: {
      deleteFunction(jsonPath, args[3]);
      break;
    }

    case CommandOption.finish: {
      doneFunction(jsonPath, args[3]);
      break;
    }

    case CommandOption.activate: {
      openFunction(jsonPath, args[3]);
      break;
    }

    case CommandOption.help: {
      InfoFunction(jsonPath);
      break;
    }

    default: {
      InfoFunction(jsonPath);
      // We will display help when no argument is passed or invalid
    }
  }
})();

const generateID = function () {
  return Date.now().toString(36) + Math.random().toString(36).substr(2);
};

//init phase
// if json file storage exist then get args and write it to file
// else create the json file and write relevant args to file
//display menu of create todo/read all todos/ update task as completed or not/ delete a todo
async function InfoFunction(jsonPath: string) {
  //check if file exists
  try {
    await fs.access(jsonPath, constants.R_OK && constants.W_OK);
    //no need to create
  } catch {
    //create file
    await fs.writeFile(jsonPath, JSON.stringify([]), "utf-8");
  }
  const UsageText = `
        Usage :-
        $ npm run start -- add "todo item"  # Add a new todo
        $ npm run start -- ls all           # Show all todos
        $ npm run start -- ls active        # Show remaining todos
        $ npm run start -- ls completed     # Show completed todos
        $ npm run start -- del TaskID       # Delete a todo by id
        $ npm run start -- del completed    # Delete all completed
        $ npm run start -- open TaskID      # Make todo undone
        $ npm run start -- done TaskID      # Complete a todo
        $ npm run start -- help             # Show usage`;

  log.cyan(UsageText);
}

//add
async function addFunction(jsonPath: string, todoFromUser: string) {
  try {
    const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
    let task: ITodo = {
      id: generateID(),
      todo: todoFromUser,
      active: true,
    };

    dataFile.push(task);
    await stringifyDataAndWriteToFile(jsonPath, dataFile);
  } catch (err) {}
}

// list of todos
async function listFunction(jsonPath: string, flag: string) {
  const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
  let filtered: ITodo[] = [];

  // switch (flag) {
  //   case ListOption.ALL: {
  //     filtered = dataFile;
  //     break;
  //   }

  //   case ListOption.ACTIVE: {
  //     filtered = dataFile.filter((t) => t.active === true);
  //     break;
  //   }

  //   case ListOption.COMPLETED: {
  //     filtered = dataFile.filter((t) => t.active === false);
  //     break;
  //   }
  // }

  await printDataFromTodoArray(filtered, jsonPath);
}

// delete todo
async function deleteFunction(jsonPath: string, param: string) {
  const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
  const ids: string[] = dataFile.map((x) => x.id);
  let afterDelete: ITodo[] = [];
  if (param === "completed") {
    afterDelete = dataFile.filter((t) => t.active === true);
  } else if (param in ids) {
    afterDelete = dataFile.filter((t) => t.id !== param);
  }

  await stringifyDataAndWriteToFile(jsonPath, afterDelete);
}

//mark task as completed
async function doneFunction(jsonPath: string, todoId: string) {
  const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
  const afterChange: ITodo[] = dataFile.map((t) => {
    if (t.id === todoId) {
      t.active = false;
      return t;
    } else {
      return t;
    }
  });

  await stringifyDataAndWriteToFile(jsonPath, afterChange);
}

//mark task as open - not completed
async function openFunction(jsonPath: string, todoId: string) {
  const dataFile: ITodo[] = await readFileReturnDataAsArrayObjects(jsonPath);
  const afterChange: ITodo[] = dataFile.map((t) => {
    if (t.id === todoId) {
      t.active = true;
      return t;
    } else {
      return t;
    }
  });

  await stringifyDataAndWriteToFile(jsonPath, afterChange);
}

//read file
async function readFileReturnDataAsArrayObjects(jsonPathFile: string) {
  const fileInput: string = await fs.readFile(jsonPathFile, "utf-8");
  let data: ITodo[] = JSON.parse(fileInput);
  return data;
}

//write file
async function stringifyDataAndWriteToFile(jsonPathFile: string, data: ITodo[]) {
  const updatedData: string = JSON.stringify(data);
  await fs.writeFile(jsonPathFile, updatedData, "utf-8");
}

async function printDataFromTodoArray(data: ITodo[], jsonPath: string) {
  for (const todo of data) {
    if (todo.active)
      log.red(
        `Task ID: ${todo.id}\nYou need To: ${todo.todo} \nPlease complete this task`
      );
    else log.green(`Task ID: ${todo.id}\nDone: ${todo.todo} \nTask completed`);

    log.yellow("------------------------------------");
  }

  InfoFunction(jsonPath);
}
