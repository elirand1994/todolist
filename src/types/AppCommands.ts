interface ICommand {
    command : string,
    params : string[]
}

export const CommandOptionsByUser : ICommand[] = [
    {command: 'add', params:[] },
    {command: 'ls', params:['-a', '-all', '-o','-open','-c','-completed'] },
    {command: 'del', params:['-completed'] },
    {command: 'activate', params:[] },
    {command: 'finish', params:[] },
    {command: 'help', params:[] },
]


export enum CommandOption {
    add = 'add',
    ls = 'ls',
    del = 'del',
    activate = 'activate',
    finish = 'finish',
    help = 'help'
}