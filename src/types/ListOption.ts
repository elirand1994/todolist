export enum ListOption {
    a = 'a',
    all = 'all',
    o = 'o',
    open = 'open',
    c = 'c',
    completed = 'completed',
}