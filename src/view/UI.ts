import log from "@ajar/marker";
import { ITodo } from "../types/index.js";

export async function printMenu() {
    const UsageText = `
    Add Task:
        add "todo Item"     #add new task to list, will be undone by default, insert todo with apostrophes

    Show Tasks:
        ls -a                  # show all tasks in list
        ls -all                # show all tasks in list
        ls -o                  # show only active tasks
        ls -open               # show only active tasks
        ls -c                  # show only completed tasks
        ls -completed          # show only completed tasks

    Delete Task:
        del TASK_ID            # deleting task by ID (ID can be found by show tasks command)
        del -completed         # deleting all completed tasks from list

    Activate Task:
        activate TASK_ID       #mark task with ID to be active

    Finish Task:
        finish TASK_ID         #mark task with ID to be completed

    Help:
        help             #show help menu to see how to use the app
    `;

  log.cyan(UsageText);
}


export async function printDataFromTodoArray(data: ITodo[]) {
  // console.log(data);
  
    for (const todo of data) {
      if (todo.active)
        log.red(
          `Task ID: ${todo.id}\nYou need To: ${todo.todo} \nPlease complete this task`
        );
      else log.green(`Task ID: ${todo.id}\nDone: ${todo.todo} \nTask completed`);
  
      log.yellow("------------------------------------");
    }
  }