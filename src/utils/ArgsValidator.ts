import { CommandOptionsByUser, CommandOption } from "../types/AppCommands.js";
import { IOptions } from "./ArgsParser.js";
import { generateID } from "./IdGenerator.js";


export function validateArgs(selectedOptionsByUser:IOptions):boolean{    
    // console.log('selectedOptionsByUser ',selectedOptionsByUser);
    if(selectedOptionsByUser.command in CommandOption){
        const paramArr : string[] = []
        CommandOptionsByUser.forEach((item,index) => {
            if(item.params.length>0 && item.command === selectedOptionsByUser.command){
                paramArr.push(...item.params)
            }
        })
        //add -> param need to be longer than 0
        // ls/del -> param in params
        //update/del -> param with length of generateID
        // help -> param undefined
        // console.log('paramArr ', paramArr);
        
        const idLength = generateID().length
        // console.log(idLength);
        if(selectedOptionsByUser.param && selectedOptionsByUser.param.length>0){ //add/ls/del/finish/activate/
            if(paramArr.includes(selectedOptionsByUser.param)){ // ls/del
                return true
            }
            else if(selectedOptionsByUser.param.length === idLength){ // del/finish/activate
                return true
            }else if(selectedOptionsByUser.command === 'add'){
                return true
            }
        }else if(selectedOptionsByUser.command === 'help'){
            return true
        }
    }
    return false
}

