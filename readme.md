Welcome To Command Line Interface (CLI) Todo-List

This app will let you the manage your daily tasks by adding, deleting, showing relevant tasks etc.

Every action will start with "npm run start" which will run the app
for example : npm run start -- ls -a 
will show the list of all tasks

Possible arguments to pass the CLI to manage your Todo List:

Add Task:
    add "todo Item"     #add new task to list, will be undone by default, insert todo with apostrophes

Show Tasks:
    ls                     # show all tasks in list by default
    ls -a                  # show all tasks in list
    ls -all                # show all tasks in list
    ls -o                  # show only active tasks
    ls -open               # show only active tasks
    ls -c                  # show only completed tasks
    ls -completed          # show only completed tasks

Delete Task:
    del TASK_ID            # deleting task by ID (ID can be found by show tasks command)
    del -completed         # deleting all completed tasks from list

Activate Task:
    activate TASK_ID       #mark task with ID to be active

Finish Task:
    finish TASK_ID         #mark task with ID to be completed

Help:
    help             #show help menu to see how to use the app